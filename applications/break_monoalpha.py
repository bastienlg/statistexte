#!/usr/bin/env python3

# Monoalphabetic substitution cipher breaker
# Markov Chain Monte Carlo (MCMC) approach
# Ref.:
#  Diaconis, The Markov Chain Monte Carlo Revolution, 2009
#  https://www.ams.org/journals/bull/2009-46-02/S0273-0979-08-01238-X/S0273-0979-08-01238-X.pdf

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
import argparse
from math import log
import random
from statistexte import *


# normalized log(proba to generate text with chain)
def probalog(chain, level, text):
    logp = 0.
    for i in range(len(text)):
        curr = text[i]
        pred = text[max(0, i-level+1):i]
        while pred not in chain.transitions[len(pred)]:
            pred = pred[1:]
        trans = chain.transitions[len(pred)][pred]
        try:
            l = trans.labels.index(curr)
            logp += log(trans.counts[l]/trans.total)
        except ValueError:
            # tradeoff to avoid log(0) = -inf
            logp += log(1./(trans.total+len(chain.alphabet)))
    return logp/len(text)


def sub(alph, perm, text):
    return ''.join(perm[alph.index(c)] for c in text)

def chain_attack(chain, level, ciph, alph=None, perm=None):
    if alph is None:
        alph = ''.join(sorted(set(ciph)))
        perm = None
    if perm is None:
        perm = list(alph)
    pciph = sub(alph, perm, ciph)
    lpopt = probalog(chain, level, pciph)
    iterations = steps = 0
    while True:
        iterations += 1
        i,j = random.sample(range(len(alph)), 2)
        perm[i],perm[j] = perm[j],perm[i]
        pciph = sub(alph, perm, ciph)
        lp = probalog(chain, level, pciph)
        if lp > lpopt or log(random.random()) < len(ciph)*(lp-lpopt):
            lpopt = lp
            steps += 1
            print()
            print(f'level {level} - {steps} steps / {iterations} iterations')
            print(f'norm. log proba.= {lpopt:.6f}')
            print(f'{repr(alph)} -> {repr("".join(perm))}')
            print(pciph)
        else:
            perm[i],perm[j] = perm[j],perm[i]


def _main():
    parser = argparse.ArgumentParser(description='statistexte MCMC - monoalphabetic substitution breaker')
    parser.add_argument('ciphertext',
                        help='ciphertext file')
    parser.add_argument('--datafile', '-d', required=True,
                        help='statistexte datafile')
    parser.add_argument('--level', '-l', type=int, default=2,
                        help='level of n-grams to use (default: 2)')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    assert chain.mode == 'alpha', 'invalid mode'
    level = args.level
    assert 0 < level <= chain.level, 'invalid level'

    with open(args.ciphertext, 'r') as f:
        ciph = chain.tokenize(f.read())  # mode alpha => filtering to chain.alpha
    alph = ''.join(sorted(set(ciph)))

    perm = list(alph)
    random.shuffle(perm)
    while True:
        try:
            chain_attack(chain, level, ciph, alph, perm)
        except KeyboardInterrupt:
            next_level = min(level+1, chain.level)
            l = input(f'\nResume with another level [1-{chain.level}] (default {next_level}) or [q]uit? ')
            l = l.strip()
            if not l:
                level = next_level
            else:
                try:
                    level = int(l)
                except ValueError:
                    break
            assert 0 < level <= chain.level, 'invalid level'
            first = False


if __name__=='__main__':
    _main()
