#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
import argparse
from arithmetic_coding import arithmetic_encode, arithmetic_decode_shortest
from statistexte import *


## Main
def _main():
    parser = argparse.ArgumentParser(description='statistexte - arithmetic coding-based steganographic mimic function')
    parser.add_argument('--input', '-i', default='-',
                        help='input text file')
    parser.add_argument('--output', '-o', default='-',
                        help='output text file')
    parser.add_argument('--encdata', '-e', required=True,
                        help='statistexte datafile to use to encode')
    parser.add_argument('--decdata', '-d', required=True,
                        help='statistexte datafile to use to decode')
    parser.add_argument('--enclvl', '-el', type=int,
                        help='encoder level (default: max)')
    parser.add_argument('--declvl', '-dl', type=int,
                        help='decoder level (default: max)')
    args = parser.parse_args()

    enchain = load_chain(args.encdata)
    if args.enclvl is not None:
        assert 0 < args.enclvl <= enchain.level, 'invalid level'
    else:
        args.enclvl = enchain.level
        print(f'Using encoder max level: {args.enclvl}', file=sys.stderr)

    dechain = load_chain(args.decdata)
    if args.declvl is not None:
        assert 0 < args.declvl <= dechain.level, 'invalid level'
    else:
        args.declvl = dechain.level
        print(f'Using decoder max level: {args.declvl}', file=sys.stderr)

    if args.input == '-':
        text = sys.stdin.read()
    else:
        with open(args.input, 'r') as f:
            text = f.read()

    print('Encoding...', end=' ', flush=True, file=sys.stderr)
    code = arithmetic_encode(enchain, args.enclvl, enchain.tokenize(text))
    print('OK', file=sys.stderr)

    print('Decoding...', end=' ', flush=True, file=sys.stderr)
    out = dechain.join(arithmetic_decode_shortest(dechain, args.declvl, code))
    print('OK', file=sys.stderr)

    if args.output == '-':
        sys.stdout.write(out)
    else:
        assert not os.path.isfile(args.output), 'output text file already exists'
        with open(args.output, 'w') as f:
            f.write(out)


if __name__=='__main__':
    _main()
