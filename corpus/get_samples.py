#!/usr/bin/env python3

CORPUS = (
    ('fr', 'Zola', 'La Bete Humaine', 'https://www.gutenberg.org/cache/epub/5154/pg5154.txt'),
    ('fr', 'Zola', 'Germinal', 'https://www.gutenberg.org/cache/epub/5711/pg5711.txt'),
    ('fr', 'La Fontaine', 'Fables 1', 'https://www.gutenberg.org/cache/epub/17941/pg17941.txt'),
    ('fr', 'La Fontaine', 'Fables 2', 'https://www.gutenberg.org/cache/epub/17942/pg17942.txt'),
    ('fr', 'Proust', 'Un Amour de Swan', 'https://www.gutenberg.org/files/62100/62100-0.txt'),
    ('en', 'Dickens', 'Great Expectations', 'https://www.gutenberg.org/files/1400/1400-0.txt'),
    ('en', 'Austen', 'Pride and Prejudice', 'https://www.gutenberg.org/files/1342/1342-0.txt'),
    ('en', 'Stevenson', 'Treasure Island', 'https://www.gutenberg.org/cache/epub/120/pg120.txt'),
    ('en', 'Melville', 'Moby Dick', 'https://www.gutenberg.org/files/2701/2701-0.txt'),
    ('en', 'E. Bronte', 'Wuthering Heights', 'https://www.gutenberg.org/cache/epub/768/pg768.txt'),
    ('en', 'C. Bronte', 'Jane Eyre', 'https://www.gutenberg.org/files/1260/1260-0.txt')
)


import re
import requests

RBEGIN = re.compile(r'\*\*\* START OF TH(E|IS) PROJECT GUTENBERG EBOOK')
REND   = re.compile(r'(\*\*\* END OF TH(E|IS) PROJECT GUTENBERG EBOOK|End of the Project Gutenberg EBook)')

def dl(url, filename):
    print(f'Downloading {url}...', end=' ', flush=True)
    req = requests.get(url)
    req.encoding = 'utf-8'
    print('ok')

    text = []
    keep = False
    for line in req.text.splitlines():
        if keep:
            if REND.match(line):
                break
            if not len(text)==len(line)==0:
                text.append(line)
        elif RBEGIN.match(line):
            keep = True
    while len(text[-1])==0:
        text.pop()

    # remove additional footer
    if text[-1].startswith('End of '):
        text.pop()
        while len(text[-1])==0:
            text.pop()

    text.append('')
    text = '\n'.join(text)

    with open(filename, 'w') as f:
        f.write(text)
    print(f'Saved as {filename} ({round(len(text)/(1<<10))}KB)')


def reform(name):
    return re.sub(r'\W+', '', name.lower().replace(' ', '_'))


if __name__=='__main__':
    for lang, author, title, url in CORPUS:
        filename = f'{lang}-{reform(author)}-{reform(title)}.txt'
        dl(url, filename)
