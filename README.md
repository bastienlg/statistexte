# statistexte

Python tools to play with textual markov chains.

## General CLI usage
### Markov chain computation
`./statistexte.py` generates/updates markov chains from text files.

#### Modes
* `alpha:<alphabet>:<level>` to create a new chain in _alphabetic_ mode (tokens are unicode characters), `<alphabet>` is the ID of the alphabet to use (empty or `all` to use all the characters from the input text files);
* `split:<regex>:<level>` to create a new chain in _arbitrary token_ mode (tokens are arbitrary strings), `<regex>` is the ID of the regex use to split the string into a sequence of tokens;
* `update` to update an existing chain.

At the beginning of `statistexte.py`, check out (or edit):
* the `ALPHA / REGEX` section to get (or set) the values for `<alphabet>`/`<regex>`;
* the `PRE-FILTERS` section to get (or set) the values for the `--prefilter` argument.

#### Populate corpus
```
$ cd corpus
$ ./get_samples.py
```

#### Generate chains from corpus
```
$ ./statistexte.py alpha:alpha_lower:4 -f lower -t corpus/fr-* -d data/frlower4
$ ./statistexte.py alpha:alphaspc_lower:4 -f lower merge_whitespaces -t corpus/fr-* -d data/frlowerspc4
$ ./statistexte.py alpha:alpha_lower:4 -f lower -t corpus/en-* -d data/enlower4
$ ./statistexte.py alpha:alphaspc_lower:4 -f lower merge_whitespaces -t corpus/en-* -d data/enlowerspc4
$ ./statistexte.py alpha::4 -t corpus/lorem_ipsum.txt -d data/lorema4
$ ./statistexte.py split::3 -t corpus/lorem_ipsum.txt -d data/loremw3
```

#### Chain information
```
$ ./chaininfo.py
== statistexte chain datafile v1 ===============================================
 mode:     alpha
 alphabet: 'abcdefghijklmnopqrstuvwxyz'
 level:    4
 files (sha1):
  1. c_bronte-jane_eyre.txt (71b10e4c71910642724acc4f16425423b41b2b92)
  2. e_bronte-wuthering_heights.txt (853d66ff171d13d45eb58f446e036dfbc58b1aab)
  3. melville-moby_dick.txt (779cf152c14509d61a665a16f474cbb2c1d2b276)
  4. dickens-great_expectations.txt (e83fa5b0dc64bf13402ef76143e9337bdafddb39)
  5. stevenson-treasure_island.txt (67dc14fd180c44f93025be8ae4de688dfac209fe)
== general statistics ==========================================================
 levels:
  1. 1 pred. / 26 trans. / 3279609 occur.
  2. 26 pred. / 609 trans. / 3279604 occur.
  3. 609 pred. / 8199 trans. / 3279599 occur.
  4. 8199 pred. / 67923 trans. / 3279594 occur.
================================================================================
```

#### Bigrams statistics
```
$ plot_bigrams.py data/frlower4
```
![bigrams fr](pics/bifr.png)

#### Chain graph
Let us first provide an abstract illustration.
```
echo -n 0110100110010110100101100110100110010110011010010110100110010110100101100110100101101001100101100110100110010110100101100110100110010110011010010110100110010110011010011001011010010110011010010110100110010110100101100110100110010110011010010110100110010110100101100110100101101001100101100110100110010110100101100110100101101001100101101001011001101001100101100110100101101001100101100110100110010110100101100110100110010110011010010110100110010110100101100110100101101001100101100110100110010110100101100110100110010110011010010110100110010110011010011001011010010110011010010110100110010110100101100110100110010110011010010110100110010110011010011001011010010110011010011001011001101001011010011001011010010110011010010110100110010110011010011001011010010110011010010110100110010110100101100110100110010110011010010110100110010110100101100110100101101001100101100110100110010110100101100110100110010110011010010110100110010110011010011001011010010110011010010110100110010110100101100110100110010110011010010110100110010110 > /tmp/tm
$ ./statistexte.py alpha::5 -t /tmp/tm -d /tmp/dtm
$ ./chain2dot.py /tmp/dtm -l 4 > /tmp/tm.dot
$ dot -Tpng /tmp/tm.dot > pics/tm.png
```
![thue-morse](pics/tm.png)

Now for instance export the `chaine_titre` from `examples/fable_generator.py` (adding a `save_chain(chaine_titre, '/tmp/titre')`).
```
$ chain2dot.py /tmp/titre -l2 > /tmp/titre.dot
$ dot -Tpdf /tmp/titre.dot > pics/titre.pdf
```
Resulting [graph](pics/titre.pdf).

### Random text generation
```
$ ./text_generator.py data/loremw3
Nulla tincidunt tincidunt mi Curabitur iaculis lorem vel rhoncus faucibus felis magna fermentum augue et ultricies lacus lorem varius purus Curabitur eu amet
```

## Specific use as a lib.
`examples/` scripts illustrate how to use statistexte as a lib. for some specific chain building.

```
$ ./examples/fable_generator.py
Construction de la chaîne... ok

La Cigale et l'Âne chargé d'éponges et les deux mulets

C'est ce que l'on en vienne aux coups;
Il faut que chaque Électeur peut de monde fournir;
Et cela me fait souvenir
D'une aventure étrange, et qui s'insinue avec
d'autant plus que les abeilles,
Avaient longtemps paru. Mais quoi! dans les productions de l'esprit, qui ne tienne pour fabuleuse celle que Planude nous a laissée.
On s'imagine que cet auteur a voulu donner à toutes sortes de
sujets, même les poissons:
Ce qu'ils disent s'adresse à tous plus salutaire.
La difficulté fut d'attacher le grelot.
L'un dit: «Je sais par renommée
Ce que la terre
Qui ne nous proposerait à imiter que les dieux l'ont quelquefois payée.
```

## Applications

`applications/` scripts explore a few interesting applications.

### To cryptography
#### Break monoalphabetic substitution ciphers
Using the [Markov Chain Monte Carlo (MCMC)](https://www.ams.org/journals/bull/2009-46-02/S0273-0979-08-01238-X/S0273-0979-08-01238-X.pdf) method.
```
$ echo -n yrffnatybgfybatfqrfivbybafqrynhgbzarharyrtraqrgranprcbchynevfrrqnafyrfnaarrffbvknagrcneyrwbheanyvfgrpbearyvhfelnacerfragrprzrffntrraqrhkcnegvrfpbzzrynaabaprdhvnhenvgrgrsnvgrnyrafrzoyrqrynerfvfgnaprsenapnvfrdhryrqronedhrzragqrabeznaqvrnhenvgyvrhqnafyrfurherffhvinagrfcbegrmprivrhkjuvfxlnhwhtroybaqdhvshzr > /tmp/ciph
$ ./applications/break_monoalpha.py /tmp/ciph -d data/frlower4
...
level 2 - 323 steps / 20979 iterations
norm. log proba.= -2.937281
'abcdefghijklmnopqrstuvwxyz' -> 'endvistaqxyzklhgprfbjumwoc'
orsslebontsonebsprsqunonesprolatnceraerorbreprtrelgrdndaoliusrrplesorsleerrssnuyletrdliormnaieloustrgnierouasizledirsretrgrcrsslbrrepraydlitursgnccroleenegrvaulailutrtrflutrloresrchorprolirsustlegrfileglusrvarorprhlivarcretpreniclepurlailutouraplesorsjrairssauqletrsdnitrkgrqurayxjuswzlamabrhonepvaufacr
^C
Resume with another level [1-4] (default 3) or [q]uit? 3
...
level 3 - 102 steps / 2215 iterations
norm. log proba.= -2.246297
'abcdefghijklmnopqrstuvwxyz' -> 'nopqrstuvyxwzabcdefghijklm'
lessanglotslongsdesviolonsdelautomneunelegendetenacepopulariseedanslesanneessoixanteparlejournalistecorneliusrwanpresentecemessageendeuxpartiescommelannoncequiauraitetefaitealensembledelaresistancefrancaisequeledebarquementdenormandieauraitlieudanslesheuressuivantesportezcevieuxyhiskwaujugeblondquifume
```

#### Password strength
Generate a chain from [leaked passwords](https://github.com/danielmiessler/SecLists) and use the probability to generate a candidate password as a measure of its strength.
```
$ ./applications/pwd_strength.py
```
![pwd strength](pics/pwd.png)

### To compression / entropy coding
#### Markov chain [arithmetic coding](https://en.wikipedia.org/wiki/Arithmetic_coding)
Naive implementation of a natural combination of [markov chain compression](https://www.youtube.com/watch?v=05RFEGWNxts) with [arithmetic coding](https://www.youtube.com/watch?v=FdMoL3PzmSA) (instead of Huffman coding for instance). At level 1, this pure arithmetic coding.
```
$ echo -n drink a cup of tea have a nice breakfast do not forget to attack at dawn > /tmp/msg
$ ./applications/arithmetic_coding.py test -d data/enlowerspc4 -t /tmp/msg
Using max level: 4
Encoding... 72 tokens (72B) -> 23B
Decoding & checking... OK
```

### To steganography

#### Hiding by decoding
Use arithmetic decoder to hide a message (at the price of a size expansion).
```
$ echo -n attack at dawn > /tmp/msg
$ ./applications/hide_by_decoding.py hide -m /tmp/msg -d data/frlowerspc4 -l4 -s /tmp/hidden
Hiding (decoding)...
Verifying... OK
$ cat /tmp/hidden
s moi a ouveries et en anxie quellesque lchandait leur du pieds cle cependsmoi main nesterrivantenant trouvement et de lorsques clargieu simplementrenti
$ ./applications/hide_by_decoding.py reveal -m - -d data/frlowerspc4 -l4 -s /tmp/hidden
Revealing (encoding)... OK
attack at dawn
```

```
$ echo -n attack at dawn > /tmp/msg
$ ./applications/hide_by_decoding.py hide -m /tmp/msg -d data/loremw3 -l1 -s /tmp/hidden
Hiding (decoding)...
Verifying... OK
$ cat /tmp/hidden
mauris turpis ultricies massa Aenean et at dui erat Pellentesque at Fusce iaculis Proin enim tempus orci congue augue Proin Aenean vitae sit ut enim tristique felis Curabitur massa ipsum ac cubilia augue varius vel diam euismod lectus sed ultrices scelerisque cubilia Pellentesque adipiscing erat eu adipiscing ante purus
$ ./applications/hide_by_decoding.py reveal -m - -d data/loremw3 -l1 -s /tmp/hidden
Revealing (encoding)... OK
attack at dawn
```

#### Mimic functions
Steganographic [mimic functions](https://www.funet.fi/pub/crypt/old/mimic/mimic.text) using markov chain arithmetic coding (instead of Huffman coding as in the article). Given a source encoder, typically corresponding to the natural language of the original message, and a different target decoder, e.g corresponding to another natural language or a specific type of content (e.g. [spam](https://www.spammimic.com/)), encode & decode to mimic the statistical properties of the target. This can be used to fool statistical tools (e.g. index of coincidence, frequency analysis, etc).
```
$ ./applications/mimic_stegano.py -e data/enlowerspc4 -el 4 -d data/frlowerspc4 -dl 4 -i msg -o /tmp/hidden
Encoding... OK
Decoding... OK
$ cat /tmp/hidden
e celaisse du moint lapiers les si une le les droitils nouvernier se dune ctait dune se un
$ ./applications/mimic_stegano.py -d data/enlowerspc4 -dl 4 -e data/frlowerspc4 -el 4 -o - -i /tmp/hidden
Encoding... OK
Decoding... OK
drink a cup of tea have a nice breakfast do not forget to attack at dawn a per 
```

```
$ ./applications/mimic_stegano.py -e data/enlowerspc4 -el 4 -d data/loremw3 -dl 1 -i msg -o /tmp/hidden
Encoding... OK
Decoding... OK
$ cat /tmp/hidden
dui nec Aliquam sapien mi Cras mauris Proin amet faucibus mi consequat ullamcorper porttitor Mauris amet ultricies Nulla vitae lorem ante Mauris enim imperdiet in Cras purus Cras
$ ./applications/mimic_stegano.py -d data/enlowerspc4 -dl 4 -e data/loremw3 -el 1 -o - -i /tmp/hidden
Encoding... OK
Decoding... OK
drink a cup of tea have a nice breakfast do not forget to attack at dawned after 
```
