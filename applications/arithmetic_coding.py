#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
import argparse
#from fractions import Fraction
import gmpy2
Fraction = gmpy2.mpq
import random
from statistexte import *


## Encoding
def arithmetic_coding_interval(chain, level, tokens):
    fl = Fraction(0)
    fd = Fraction(1)
    for i in range(len(tokens)):
        pred = tokens[max(0, i-level+1):i]
        while pred not in chain.transitions[len(pred)]:
            pred = pred[1:]
        trans = chain.transitions[len(pred)].get(pred, None)
        assert trans is not None, 'the chain cannot generate this text'
        try:
            j = trans.labels.index(tokens[i])
        except:
            assert False, 'the chain cannot generate this text'
        fl += Fraction(sum(trans.counts[:j]), trans.total) * fd
        fd *= Fraction(trans.counts[j], trans.total)
    return (fl, fl+fd)

def coding_interval_to_bytes(fl, fr):
    pl,ql = fl.numerator, fl.denominator
    pr,qr = fr.numerator, fr.denominator
    out = bytearray()
    while True:
        al,pl = divmod(256*pl, ql)
        ar,pr = divmod(256*pr, qr)
        if al==ar:
            out.append(al)
        elif al+1==ar:
            bl,pl = divmod(256*pl, ql)
            cl = 256*al+bl+1
            out.extend(divmod(cl, 256))
            break
        else:
            out.append(random.randint(al+1,ar-1))
            break
    return out

def arithmetic_encode(chain, level, tokens, prepend_length=False):
    fl,fr = arithmetic_coding_interval(chain, level, tokens)
    data = coding_interval_to_bytes(fl, fr)
    if prepend_length:
        data = len(tokens).to_bytes(4, 'big') + data
    return data


## Decoding
def bytes_to_code(data):
    return Fraction(int.from_bytes(data, 'big'), 256**len(data))

def arithmetic_decode_fraction(chain, level, code, length):
    out = []
    for i in range(length):
        pred = chain.norm(out[max(0, i-level+1):i])
        while pred not in chain.transitions[len(pred)]:
            pred = pred[1:]
        trans = chain.transitions[len(pred)].get(pred, None)
        if trans is None:  # terminal reached
            break
        l = Fraction(0)
        for j in range(len(trans.counts)):
            d = Fraction(trans.counts[j], trans.total)
            if code < l+d:
                code = (code-l)/d
                out.append(trans.labels[j])
                break
            l += d
    return chain.norm(out)

def arithmetic_decode(chain, level, data, length=None):
    if length is None:
        length = int.from_bytes(data[:4], 'big')
        data = data[4:]
    code = bytes_to_code(data)
    return arithmetic_decode_fraction(chain, level, code, length)


def arithmetic_decode_shortest(chain, level, data):
    code = bytes_to_code(data)
    out = []
    fl = Fraction(0)
    fd = Fraction(1)
    feps = Fraction(1, 256**len(data))
    while True:
        i = len(out)
        pred = chain.norm(out[max(0, i-level+1):i])
        while pred not in chain.transitions[len(pred)]:
            pred = pred[1:]
        trans = chain.transitions[len(pred)].get(pred, None)
        if trans is None:  # terminal reached
            break
        l = Fraction(0)
        for j in range(len(trans.counts)):
            d = Fraction(trans.counts[j], trans.total)
            if code < l+d:
                code = (code-l)/d
                out.append(trans.labels[j])
                fl += l*fd
                fd *= d
                break
            l += d
        if fd < feps and \
           coding_interval_to_bytes(fl, fl+fd).startswith(data):
            break
    return chain.norm(out)


## Main
def _main():
    parser = argparse.ArgumentParser(description='statistexte - MC-based (toy) arithmetic encoder')
    parser.add_argument('mode', choices=('encode', 'decode', 'test'),
                        help='mode')
    parser.add_argument('--textfile', '-t', required=True,
                        help='input textfile in encode mode / output textfile in decode mode')
    parser.add_argument('--datafile', '-d', required=True,
                        help='statistexte datafile to use to encode/decode')
    parser.add_argument('--encfile', '-e',
                        help='output encoded file in encode mode / input encoded file in decode mode')
    parser.add_argument('--level', '-l', type=int,
                        help='level to use (default: max)')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    if args.level is not None:
        assert 0 < args.level <= chain.level, 'invalid level'
    else:
        args.level = chain.level
        print(f'Using max level: {args.level}')

    if args.mode == 'encode':
        assert args.encfile is not None, f'encoded file (--encfile) not provided'
        assert not os.path.isfile(args.encfile), f'output file already exists: {args.encfile}'
        with open(args.textfile, 'r') as f:
            tokens = chain.tokenize(f.read())

        print('Encoding...')
        encdata = arithmetic_encode(chain, args.level, tokens, True)

        with open(args.encfile, 'wb') as f:
            f.write(encdata)
        print(f'{args.textfile} {len(tokens)} tokens ({len(chain.join(tokens).encode("utf-8"))}B) -> {len(encdata)}B {args.encfile}')

    elif args.mode == 'decode':
        assert not os.path.isfile(args.textfile), f'output file already exists: {args.textfile}'
        with open(args.encfile, 'rb') as f:
            encdata = f.read()

        print('Decoding...')
        text = chain.join(arithmetic_decode(chain, args.level, encdata))
        with open(args.textfile, 'w') as f:
            f.write(text)
        print(f'{args.encfile} {len(encdata)}B -> {len(text)} chars ({len(text.encode("utf-8"))}B) {args.textfile}')

    else:
        with open(args.textfile, 'r') as f:
            tokens = chain.tokenize(f.read())

        print('Encoding...', end=' ', flush=True)
        encdata = arithmetic_encode(chain, args.level, tokens)
        print(f'{len(tokens)} tokens ({len(chain.join(tokens).encode("utf-8"))}B) -> {len(encdata)}B')

        print('Decoding & checking...', end=' ', flush=True)
        out = arithmetic_decode(chain, args.level, encdata, len(tokens))
        assert out == tokens
        print('OK')


if __name__=='__main__':
    _main()
