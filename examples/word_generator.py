#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
from statistexte import *
from text_generator import chain_random_gen
import argparse


DEFAULT_WORDLIST = '/usr/share/dict/words'
DEFAULT_LEVEL = 4


def load_dict(wordlist=DEFAULT_WORDLIST, level=DEFAULT_LEVEL):
    chain = ChainData('alpha', 'all', level)
    with open(wordlist, 'r') as words:
        for word in words.readlines():
            chain.update(None, f'^{word.strip()}$')
    chain.freeze()
    return chain


def _main():
    parser = argparse.ArgumentParser(description='statistexte markov chains - random word generator')
    parser.add_argument('--wordlist', '-w', default=DEFAULT_WORDLIST,
                        help=f'dictionary file (default: {DEFAULT_WORDLIST})')
    parser.add_argument('--level', '-l', type=int, default=DEFAULT_LEVEL,
                        help=f'level of the generated chain (default: {DEFAULT_LEVEL})')
    parser.add_argument('--number', '-n', type=int, default=1<<4,
                        help='number of words to generate')
    args = parser.parse_args()
    assert args.level > 1, 'invalid level'

    print('Loading dict...', end=' ', flush=True, file=sys.stderr)
    chain = load_dict(args.wordlist, args.level)
    print('ok', file=sys.stderr)

    for _ in range(args.number):
        word = chain_random_gen(chain, chain.level, start='^')[1:].rstrip('$')
        print(word)


if __name__=='__main__':
    _main()
