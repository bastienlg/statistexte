#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
from statistexte import *
from text_generator import chain_random_gen


FABLES = ((f'{path}/corpus/fr-la_fontaine-fables_1.txt', 'A Monseigneur le '),
          (f'{path}/corpus/fr-la_fontaine-fables_2.txt', 'Contre ceux qui '))
LEVEL = 3


_SYMBOLE0 = '^^DEBUT^^'

def construire_chaines():
    chaine_titre = ChainData('split', 'linespaces', LEVEL-1)
    chaine_corps = ChainData('split', 'linespaces', LEVEL)
    for fichier, debut in FABLES:
        with open(fichier, 'r') as f:
            texte = f.read()
        texte = texte[texte.index(debut):].replace('_', '')
        texte = texte.split('\n\n')
        for bloc in texte:
            bloc = bloc.strip('\n').replace('\n', '\n ').strip(' ')
            if bloc:
                if '\n' not in bloc:
                    bloc = _SYMBOLE0 + ' ' + bloc
                    chaine_titre.update(None, bloc)
                else:
                    bloc = _SYMBOLE0 + ' ' + bloc + '\n'
                    chaine_corps.update(None, bloc)
    chaine_titre.freeze()
    chaine_corps.freeze()
    return chaine_titre, chaine_corps


def _main():
    print('Construction de la chaîne...', end=' ', flush=True, file=sys.stderr)
    chaine_titre, chaine_corps = construire_chaines()
    print('ok\n', file=sys.stderr)

    titre = chain_random_gen(chaine_titre, start=[_SYMBOLE0], text_output=True)
    titre = titre[len(_SYMBOLE0)+1:]
    fable = chain_random_gen(chaine_corps, start=[_SYMBOLE0], text_output=True)
    fable = fable[len(_SYMBOLE0)+1:].replace('\n ', '\n')
    print(titre)
    print()
    print(fable)


if __name__=='__main__':
    _main()
