#!/usr/bin/env python3

import argparse
import json
from statistexte import *
import sys


def dot(chain, level):
    assert 0 < level <= chain.level, 'invalid level'
    DOT = [f'# {" ".join(sys.argv)}',
           'digraph {']
    for l,trans in chain.transitions[level-1].items():
        labl = json.dumps(chain.join(l))
        for i,(r,c) in enumerate(zip(trans.labels, trans.counts)):
            labr = json.dumps(chain.join(l[1:]+chain.norm((r,))))
            p = round(c/trans.total, 4)
            DOT.append(f'{labl} -> {labr} [label="{p}"];')
    DOT.append('}')
    return '\n'.join(DOT)


def _main():
    parser = argparse.ArgumentParser(description='statistexte markov chains - export to Graphviz DOT format')
    parser.add_argument('datafile',
                        help='statistexte datafile')
    parser.add_argument('--level', '-l', type=int, required=True,
                        help='level to output')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    print(dot(chain, args.level))


if __name__=='__main__':
    _main()
