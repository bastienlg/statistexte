#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
import argparse
from arithmetic_coding import arithmetic_encode, arithmetic_decode_shortest
import os
from statistexte import *


_SEP_LEN = 6


def xor(a,b):
    return bytes(ai^bi for ai,bi in zip(a,b))

def steg_hide(chain, level, msg):
    sep = os.urandom(_SEP_LEN)
    mask = os.urandom(len(msg))
    ciph = xor(msg, mask)
    code = sep + mask + ciph + sep
    return chain.join(arithmetic_decode_shortest(chain, level, code))


def steg_reveal(chain, level, text):
    tokens = chain.tokenize(text)
    code = arithmetic_encode(chain, level, tokens)
    sep = code[:_SEP_LEN]
    try:
        mask_ciph = code[_SEP_LEN: code.index(sep, _SEP_LEN)]
        assert len(mask_ciph)%2 == 0
    except (ValueError, AssertionError):
        return None
    len_msg = len(mask_ciph)//2
    mask, ciph = mask_ciph[:len_msg], mask_ciph[len_msg:]
    msg = xor(ciph, mask)
    return msg


## Main
def _main():
    parser = argparse.ArgumentParser(description='statistexte - arithmetic coding-based steganographic tool')
    parser.add_argument('mode', choices=('hide', 'reveal'),
                        help='mode')
    parser.add_argument('--msgfile', '-m', default='-',
                        help='input secret file in hide mode / output secret file in reveal mode')
    parser.add_argument('--datafile', '-d', required=True,
                        help='statistexte datafile to use to encode/decode')
    parser.add_argument('--stegfile', '-s', default='-',
                        help='output decoded file in hide mode / input decoded file in reveal mode')
    parser.add_argument('--level', '-l', type=int,
                        help='level to use (default: max)')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    if args.level is not None:
        assert 0 < args.level <= chain.level, 'invalid level'
    else:
        args.level = chain.level
        print(f'Using max level: {args.level}', file=sys.stderr)

    if args.mode == 'hide':
        if args.msgfile == '-':
            msg = sys.stdin.buffer.read()
        else:
            with open(args.msgfile, 'rb') as f:
                msg = f.read()

        while True:
            print('Hiding (decoding)...', file=sys.stderr)
            text = steg_hide(chain, args.level, msg)

            print('Verifying...', end=' ', flush=True, file=sys.stderr)
            if steg_reveal(chain, args.level, text) == msg:
                print('OK', file=sys.stderr)
                break
            else:
                print('Fail, retrying.', file=sys.stderr)

        if args.stegfile == '-':
            sys.stdout.write(text)
        else:
            assert not os.path.isfile(args.stegfile), 'output steg. file already exists'
            with open(args.stegfile, 'w') as f:
                f.write(text)

    else:  # 'reveal':
        if args.stegfile == '-':
            text = sys.stdin.read()
        else:
            with open(args.stegfile, 'r') as f:
                text = f.read()

        print('Revealing (encoding)...', end=' ', flush=True, file=sys.stderr)
        msg = steg_reveal(chain, args.level, text)
        if msg is None:
            print('FAIL', file=sys.stderr)
            sys.exit(1)
        print('OK', file=sys.stderr)

        if args.msgfile == '-':
            sys.stdout.buffer.write(msg)
        else:
            assert not os.path.isfile(args.msgfile), 'output message file already exists'
            with open(args.msgfile, 'wb') as f:
                f.write(msg)


if __name__=='__main__':
    _main()
