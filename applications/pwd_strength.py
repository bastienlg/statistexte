#!/usr/bin/env python3

import sys, os.path
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, path)
from statistexte import *
from text_generator import chain_random_gen
from break_monoalpha import probalog
import matplotlib.pyplot as plt
import numpy as np
from os.path import expanduser
import random


GEN_LIST = expanduser('~/Sec/SecLists/Passwords/darkc0de.txt')
TEST_LIST = expanduser('~/Sec/SecLists/Passwords/Leaked-Databases/rockyou.txt')
LENGTH = 8
LEVEL = 4


def load_list(wordlistfile, wordlen=LENGTH):
    wordlist = []
    with open(wordlistfile, 'rb') as f:
        words = f.read().split(b'\n')
    for word in words:
        try:
            word = word.decode().strip()
        except UnicodeDecodeError:
            continue
        if len(word) == wordlen:
            wordlist.append(word)
    return wordlist

def make_chain(wordlist, level=LEVEL):
    chain = ChainData('alpha', 'all', level)
    for word in wordlist:
        chain.update(None, word)
    chain.freeze()
    return chain


def _main():
    print('Loading lists & building chain...', end=' ', flush=True)
    gen_list = load_list(GEN_LIST)
    chain = make_chain(gen_list)
    test_list = load_list(TEST_LIST)
    print('ok')

    chain.alphabet = chain.transitions[0][''].labels
    size = 1<<16

    ## generate data
    print('Generating data...', end=' ', flush=True)
    scores = [[] for _ in range(3)]
    test_words = random.sample(test_list, size)
    for word in test_words:
        # test
        scr = -probalog(chain, chain.level, word)
        scores[0].append(scr)

        # random (should be low)
        word = ''.join(random.choice(chain.alphabet) for _ in range(LENGTH))
        scr = -probalog(chain, chain.level, word)
        scores[1].append(scr)

        # generated (should be high)
        word = ''
        while len(word) != LENGTH:
            word = chain_random_gen(chain, maxlength=LENGTH)
        scr = -probalog(chain, chain.level, word)
        scores[2].append(scr)
    print('ok')

    ## plot data
    titles = (
        f'Strength of {size} passwords from the test list',
        f'Strength of {size} random passwords',
        f'Strength of {size} chain-generated passwords'
    )
    fig, axs = plt.subplots(len(scores), 1)
    fig.suptitle(f'Reference chain of level {LEVEL} generated from\n{len(gen_list)} leaked passwords of length {LENGTH}')
    bins = np.linspace(0, 10, 10*10+1)
    for i in range(len(scores)):
        axs[i].hist(scores[i], bins)
        axs[i].set_title(titles[i])
        axs[i].set_xlim(0, 10)
        axs[i].set_xticks(range(11))
    plt.tight_layout()
    plt.show()


if __name__=='__main__':
    _main()
