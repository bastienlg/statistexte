#!/usr/bin/env python3

import argparse
import matplotlib.pyplot as plt
from statistexte import *


def plot_bigrams(chain):
    transitions = chain.transitions[1]
    alph = sorted(transitions.keys())
    labalph = [repr(a) if len(a)!=1 or not 32<ord(a)<127 else a
               for a in alph]
    prob = [[0.]*len(alph) for _ in alph]
    freq = [[0.]*len(alph) for _ in alph]
    total = sum(trans.total for trans in transitions.values())
    for i,a in enumerate(alph):
        trans = transitions[a]
        for lbl,cnt in zip(trans.labels, trans.counts):
            j = alph.index(lbl)
            prob[i][j] = cnt/trans.total
            freq[i][j] = cnt/total
    fig, ax = plt.subplots(1,2, num='statistexte bigrams heatmaps')
    ax[0].imshow(prob)
    ax[0].set_xticks(range(len(alph)), labels=labalph, fontname='monospace')
    ax[0].set_yticks(range(len(alph)), labels=labalph, fontname='monospace')
    ax[0].set_title('bigram transition probability')
    ax[1].imshow(freq)
    ax[1].set_xticks(range(len(alph)), labels=labalph, fontname='monospace')
    ax[1].set_yticks(range(len(alph)), labels=labalph, fontname='monospace')
    ax[1].set_title('bigram frequency')
    fig.tight_layout()
    plt.show()


def _main():
    parser = argparse.ArgumentParser(description='statistexte - bigrams heatmaps')
    parser.add_argument('datafile',
                        help='statistexte datafile')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    assert chain.level >= 2, 'no bigrams in chain'
    plot_bigrams(chain)


if __name__=='__main__':
    _main()
