#!/usr/bin/env python3

import argparse
import os.path
import random
from statistexte import *
import sys


def chain_random_gen(chain, level=None, maxlength=1<<8, start=None, text_output=False):
    if level is None:
        level = chain.level
    if start is not None:
        out = list(start)
    else:
        out = []
    for l in range(len(out), maxlength):
        pred = chain.norm(out[max(0, l-level+1):])
        trans = chain.transitions[len(pred)].get(pred, None)
        if trans is None:  # terminal reached
            break
        cnt = 0
        p = random.random()
        for i,c in enumerate(trans.counts):
            cnt += c
            if cnt/trans.total > p:
                out.append(trans.labels[i])
                break
    if text_output:
        return chain.join(out)
    return chain.norm(out)


def _main():
    parser = argparse.ArgumentParser(description='statistexte markov chains - random text generator')
    parser.add_argument('datafile',
                        help='statistexte datafile')
    parser.add_argument('--size', '-s', type=int, default=1<<7,
                        help='max size (in tokens) of the generated sequence')
    parser.add_argument('--level', '-l', type=int,
                        help='level to use (default: max)')
    parser.add_argument('--seed', type=int,
                        help='RNG seed')
    parser.add_argument('--output', '-o', default='-',
                        help='output text file')
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)

    chain = load_chain(args.datafile)
    if args.level is not None:
        assert 0 < args.level <= chain.level, 'invalid level'
    else:
        args.level = chain.level

    out = chain_random_gen(chain, args.level, args.size, text_output=True)
    if args.output == '-':
        sys.stdout.write(out)
    else:
        assert not os.path.isfile(args.output), 'output text file already exists'
        with open(args.output, 'w') as f:
            f.write(out)


if __name__=='__main__':
    _main()
