#!/usr/bin/env python3

import re
import string
import unicodedata

## ================= ALPHA / REGEX ================= ##
_ALPHABET = {
    '': None,                       # default: any char
    'all': None,
    'alpha': string.ascii_letters,
    'alphaspc': string.ascii_letters+string.whitespace,
    'alphanum': string.ascii_letters+string.digits,
    'alpha_lower': string.ascii_lowercase,
    'alphaspc_lower': string.ascii_lowercase+string.whitespace,
    'alpha_upper': string.ascii_uppercase,
    'alphaspc_upper': string.ascii_uppercase+string.whitespace,
    'vowels': 'aeiouy'
}

_SPLIT_REGEX = {
    # pairs (regex, separator) where the default
    # separator is used to join tokens and should
    # of course match the regex
    '': (r'\W+', ' '),              # default: words
    'words': (r'\W+', ' '),
    'spaces': (r'\s+', ' '),
    'linespaces': (r'[ \t\r]+', ' '),
    'punctuation': (f'[{re.escape(string.punctuation+string.whitespace)}]+', ' ')
}
## ================== PRE-FILTERS ================== ##
def st_remove_diacritics(s):
    nfkd = unicodedata.normalize('NFKD', s)
    return ''.join(c for c in nfkd if unicodedata.combining(c)==0)

def st_unicode_to_ascii(s):
    nfkd = unicodedata.normalize('NFKD', s)
    to_ascii = nfkd.encode('ASCII', 'ignore')
    return to_ascii.decode()

_PREFILTER = {
    'lower': str.lower,
    'upper': str.upper,
    'to_ascii': st_unicode_to_ascii,
    'remove_accents': st_remove_diacritics,
    'remove_diacritics': st_remove_diacritics,
    'merge_whitespaces': (lambda s: re.sub(r'\s+', ' ', s))
}
## ================================================= ##


import argparse
import bz2
from collections import Counter
from hashlib import sha1
import os.path
import pickle


_FORMAT_VERSION = 1

class ChainData:
    def __init__(self, mode, alrex, level):
        self.version = _FORMAT_VERSION
        self.files = []
        self.mode = mode
        if self.mode == 'alpha':
            assert alrex in _ALPHABET, f'unknown alphabet id: {alrex}'
            self.alphabet = _ALPHABET[alrex]
        else:
            assert alrex in _SPLIT_REGEX, f'unknown regex id: {alrex}'
            self.regex, self.separator = _SPLIT_REGEX[alrex]
        self.level = level
        self.transitions = [{} for _ in range(level)]

    def tokenize(self, text):
        if self.mode == 'alpha':
            if self.alphabet is None:
                tokens = text
            else:
                tokens = ''.join(c for c in text if c in self.alphabet)
        else:
            creg = re.compile(self.regex, flags=re.DOTALL|re.MULTILINE)
            tokens = creg.split(text)
            tokens = tuple(tok for tok in tokens if len(tok)>0)
        return tokens

    def norm(self, tokens):
        if self.mode == 'alpha':
            if isinstance(tokens, str):
                norm_form = tokens
            else:
                norm_form = ''.join(tokens)
        else:
            if isinstance(tokens, tuple):
                norm_form = tokens  # no copy
            elif isinstance(tokens, str):
                norm_form = (tokens,)
            else:
                norm_form = tuple(tokens)
        return norm_form

    def join(self, tokens):
        if self.mode == 'alpha':
            if isinstance(tokens, str):
                str_form = tokens
            else:
                str_form = ''.join(tokens)
        else:
            str_form = self.separator.join(tokens)
        return str_form

    def freeze(self):
        # chain must be frozen before any use after a sequence of updates
        for lvl in self.transitions:
            for key in lvl:
                if not isinstance(lvl[key], Transition):
                    lvl[key] = Transition(lvl[key], self.mode=='alpha')

    def update(self, filename, text):
        if isinstance(filename, str):
            filebase = os.path.basename(filename)
            h = sha1(text.encode('utf-8')).hexdigest()
            self.files.append((filebase, h))
        tokens = self.tokenize(text)
        for pos in range(len(tokens)):
            curr = tokens[pos]
            for lvl in range(min(self.level, pos+1)):
                pred = tokens[pos-lvl:pos]
                if pred not in self.transitions[lvl]:
                    self.transitions[lvl][pred] = Counter()
                elif isinstance(self.transitions[lvl][pred], Transition):
                    self.transitions[lvl][pred] = self.transitions[lvl][pred].to_counter()
                self.transitions[lvl][pred][curr] += 1


class Transition:
    def __init__(self, counter, alphamode=False):
        self.labels, self.counts = zip(*sorted(counter.items()))
        if alphamode:
            self.labels = ''.join(self.labels)
        self.total = sum(self.counts)

    def __repr__(self):
        return repr(tuple(zip(self.labels, self.counts)))

    def to_counter(self):
        return Counter(dict(zip(self.labels, self.counts)))


def save_chain(chain, datafile):
    chain.freeze()
    with open(datafile, 'wb') as f:
        f.write(bz2.compress(pickle.dumps(chain)))

def load_chain(datafile):
    with open(datafile, 'rb') as f:
        chain = pickle.loads(bz2.decompress(f.read()))
    assert isinstance(chain, ChainData), 'improper datafile'
    assert chain.version <= _FORMAT_VERSION, f'incompatible data version: {chain.version} > {FORMAT_VERSION}'
    return chain

def update_chain(chain, textfile, prefilters=()):
    with open(textfile, 'r') as f:  # encoding decided by the system
        text = f.read()
    for filt in prefilters:
        assert filt in _PREFILTER, f'unknown filter id: {filt}'
        text = _PREFILTER[filt](text)
    chain.update(textfile, text)


def _main():
    parser = argparse.ArgumentParser(description='textual markov chains')
    parser.add_argument('mode',
                        help='mode: alpha:<alphabet>:<level> / split:<regex>:<level> / update')
    parser.add_argument('--textfile', '-t', nargs='+',
                        help='input textfile(s)')
    parser.add_argument('--datafile', '-d', required=True,
                        help='datafile to output/update')
    parser.add_argument('--prefilter', '-f', nargs='+', default=(),
                        help='filters to apply before tokenizing')
    args = parser.parse_args()

    if args.mode == 'update':
        chain = load_chain(args.datafile)
    else:
        match = re.fullmatch(r'(alpha|split):(.*?):([1-9])', args.mode)
        assert match is not None, f'incorrect mode: {args.mode}'
        mode, alrex, level = match.groups()
        level = int(level)
        assert not os.path.isfile(args.datafile), 'output datafile already exists'
        chain = ChainData(mode, alrex, level)

    print('Creating/Updating chain:')
    for tf in args.textfile:
        print(f'> {tf}')
        update_chain(chain, tf, args.prefilter)

    print('Exporting chain...')
    save_chain(chain, args.datafile)
    print(f'Saved as {args.datafile}')


if __name__=='__main__':
    _main()
