#!/usr/bin/env python3

import argparse
from shutil import get_terminal_size
from statistexte import *


def print_info(chain, stats=False):
    width = get_terminal_size().columns
    print(f'== statistexte chain datafile v{chain.version} '.ljust(width, '='))
    print(f' mode:     {chain.mode}')
    if chain.mode == 'alpha':
        print(f' alphabet: {repr(chain.alphabet)}')
    else:
        print(f' regex:    {repr(chain.regex)}')
        print(f' sep.:     {repr(chain.separator)}')
    print(f' level:    {chain.level}')
    print(f' files (sha1):')
    for i,(f,h) in enumerate(chain.files):
        print(f'  {i+1}. {f} ({h})')
    if stats:
        print(f'== general statistics '.ljust(width, '='))
        print(' levels:')
        for l,tlvl in enumerate(chain.transitions):
            tcnt = tocc = 0
            for trans in tlvl.values():
                tcnt += len(trans.labels)
                tocc += trans.total
            print(f'  {l+1}. {len(tlvl)} pred. / {tcnt} trans. / {tocc} occur.')
    print('='*width)


def _main():
    parser = argparse.ArgumentParser(description='statistexte markov chains - general information / inspection tool')
    parser.add_argument('datafile',
                        help='statistexte datafile')
    parser.add_argument('--stats', '-s', action='store_true',
                        help='show general statistics')
    args = parser.parse_args()

    chain = load_chain(args.datafile)
    print_info(chain, args.stats)


if __name__=='__main__':
    _main()
